package stackover.email.service.service;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import stackover.email.service.dto.InviteEmailDto;
import stackover.email.service.dto.NewPasswordEmailDto;
import stackover.email.service.dto.RecoveryEmailDto;
import stackover.email.service.enums.MailLang;
import stackover.email.service.enums.MailType;
import stackover.email.service.factory.MailTemplateFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailService {

    private final MailTemplateFactory templateFactory;
    private final EmailSender emailSender;

    public void sendInviteEmail(InviteEmailDto dto) {
        sendEmail(dto.email(), dto.lang(), MailType.INVITE, Map.of("link", generateInviteLink(dto.email())));
    }

    public void sendRecoveryEmail(RecoveryEmailDto dto) {
        sendEmail(dto.email(), dto.lang(), MailType.RECOVERY_PASSWORD, Map.of("hash", dto.hash()));
    }

    public void sendNewPasswordEmail(NewPasswordEmailDto dto) {
        sendEmail(dto.email(), dto.lang(), MailType.NEW_PASSWORD, Map.of("password", dto.password()));
    }

    private void sendEmail(String email, MailLang lang, MailType type, Map<String, Object> params) {
        try {
            Template template = templateFactory.getTemplate(type, lang);
            String content = processTemplate(template, params);
            emailSender.send(email, "Ваше сообщение", content);
            log.info("Email успешно отправлен: {}", email);
        } catch (IOException | TemplateException | MessagingException e) {
            log.error("Ошибка при отправке письма на {}", email, e);
        }
    }

    private String generateInviteLink(String email) {
        return "https://example.com/invite?email=" + email; //TODO: добавить реализацию генерации ссылки
    }

    private String processTemplate(Template template, Map<String, Object> params) throws IOException, TemplateException {
        try (var writer = new java.io.StringWriter()) {
            template.process(params, writer);
            return writer.toString();
        }
    }
}