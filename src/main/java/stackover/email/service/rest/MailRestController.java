package stackover.email.service.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stackover.email.service.dto.InviteEmailDto;
import stackover.email.service.dto.NewPasswordEmailDto;
import stackover.email.service.dto.RecoveryEmailDto;
import stackover.email.service.service.MailService;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/inner/mail")
@RequiredArgsConstructor
@Tag(name = "Mail API", description = "Эндпоинты для отправки email-ов")
public class MailRestController {

    private final MailService mailService;

    @PostMapping("/invite")
    @Operation(summary = "Отправка приглашения", description = "Отправляет пользователю ссылку для подтверждения email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Приглашение успешно отправлено"),
            @ApiResponse(responseCode = "400", description = "Ошибка в переданных данных"),
            @ApiResponse(responseCode = "500", description = "Ошибка сервера при отправке email")
    })
    public ResponseEntity<String> sendEmailAboutRegistration(@RequestBody @Valid InviteEmailDto dto) {
        log.info("Отправка приглашения на email: {}", dto.email());
        try {
            mailService.sendInviteEmail(dto);
            return ResponseEntity.ok("Приглашение успешно отправлено");
        } catch (Exception e) {
            log.error("Ошибка при отправке приглашения на email {}: {}", dto.email(), e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при отправке приглашения");
        }
    }

    @PostMapping("/recovery")
    @Operation(summary = "Отправка ссылки на восстановление пароля", description = "Отправляет пользователю ссылку на восстановление пароля")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ссылка для восстановления пароля успешно отправлена"),
            @ApiResponse(responseCode = "400", description = "Ошибка в переданных данных"),
            @ApiResponse(responseCode = "500", description = "Ошибка сервера при отправке email")
    })
    public ResponseEntity<String> sendRecoveryLink(@RequestBody @Valid RecoveryEmailDto dto) {
        log.info("Отправка ссылки для восстановления пароля на email: {}", dto.email());
        try {
            mailService.sendRecoveryEmail(dto);
            return ResponseEntity.ok("Ссылка для восстановления пароля успешно отправлена");
        } catch (Exception e) {
            log.error("Ошибка при отправке ссылки восстановления на email {}: {}", dto.email(), e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при отправке ссылки восстановления");
        }
    }

    @PostMapping("/new-password")
    @Operation(summary = "Отправка нового пароля", description = "Отправляет пользователю новый пароль")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Новый пароль успешно отправлен"),
            @ApiResponse(responseCode = "400", description = "Ошибка в переданных данных"),
            @ApiResponse(responseCode = "500", description = "Ошибка сервера при отправке email")
    })
    public ResponseEntity<String> sendNewPasswordByEmail(@RequestBody @Valid NewPasswordEmailDto dto) {
        log.info("Отправка нового пароля на email: {}", dto.email());
        try {
            mailService.sendNewPasswordEmail(dto);
            return ResponseEntity.ok("Новый пароль успешно отправлен");
        } catch (Exception e) {
            log.error("Ошибка при отправке нового пароля на email {}: {}", dto.email(), e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при отправке нового пароля");
        }
    }
}
