package stackover.email.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import stackover.email.service.enums.MailLang;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Schema(description = "DTO для отправки ссылки на восстановление пароля")
public record RecoveryEmailDto(
        @NotBlank @Email
        @Schema(description = "Email пользователя", example = "user@example.com")
        String email,

        @NotNull
        @Schema(description = "Язык письма", example = "EN")
        MailLang lang,

        @NotBlank
        @Schema(description = "Хеш для подтверждения восстановления", example = "abc123xyz")
        String hash
) {
}
