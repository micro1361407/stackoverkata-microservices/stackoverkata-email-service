<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
</head>
<body>
<p>
    <span>Вы приглашены на</span>
    <b>StackOver</b>
</p>
<p>
    <span>Для регистрации на платформе перейдите по</span>
    <a href="${link}">ссылке</a>
    <span>, чтобы попасть в систему. Система предназначена для работы с компьютером/планшетом
        и не адаптирована под мобильные устройства.
        Рекомендуемый браузер - Google Chrome.</span>
</p>
</body>
</html>