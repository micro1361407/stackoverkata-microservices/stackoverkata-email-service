<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
</head>
<body>
<p>
    <span>You are invited to</span>
    <b>StackOver</b>
</p>
<p>
    <span>To register on the platform, follow to</span>
    <a href="${link}">link</a>
    <span>, to get into the system. The system is designed to work with a computer/tablet
        and it is not adapted for mobile devices.
        The recommended browser is Google Chrome.</span>
</p>
</body>
</html>