<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title></title>
</head>
<body>
<p>
    <span>Password was successfully restored</span>
</p>

<p>
    <span>Your login:</span>
    <span>${email}</span>
</p>
<p>
    <span>Your password:</span>
    <span>${password}</span>
</p>
<p>
    <span>This is your new data. You can change your password in your profile settings.</span>
</p>
</body>
</html>